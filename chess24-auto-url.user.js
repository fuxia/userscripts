// ==UserScript==
// @name         Auto-URL
// @namespace    https://fuxia.me/
// @version      0.2
// @description  Sets the URL for an article based on the title.
// @author       Fuxia Scholz
// @match        https://chess24.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    var chars = {
        ' ':'-',
        ',':'', ':':'', '?':'', '!':'', '.':'', '’':'', '“':'', '”':'', '„':'', '“':'',
        '&':'-',
        '–':'-', // n-dash
        '—':'-', // m-dash
        'ä':'ae', 'æ':'ae', 'à':'a', 'á':'a', 'â':'a', 'å ':'a', 'ã':'a',
        'é':'e', 'è':'e', 'ê':'e', 'ë':'e',
        'ï':'i', 'î':'i', 'ì':'i', 'í':'i', 'ı ':'i',
        'ö':'oe', 'œ':'oe', 'ó':'o', 'ò':'o', 'ô':'o', 'ø':'o', 'õ':'o',
        'ü':'ue', 'ù':'u', 'û':'u',
        'ß':'ss', 'ş':'s',
        'ñ':'n', 'ç':'c', 'ğ':'g',
        '€':'euro', '$':'dollar', '₤':'lira'
    };
    var re = new RegExp(`[${ Object.keys(chars).join('') }]`,'g');
    var urlfield = document.getElementById("ContentGroup_permalink");
    var btn = document.createElement("input");
    btn.classList.add('cBtn', 'cBtnWhite');
    btn.setAttribute('type', 'submit');
    btn.setAttribute('value', 'Create URL');

    btn.onclick = () => {
        var title = document.getElementById("ContentGroup_title").value;
        var url = '';

        url = title.toLowerCase();
        url = url.replace(re, m => chars[m]);
        url = url.replace(/--+/g, '-');
        urlfield.value = url;
        return false;
    };
    urlfield.after(btn);
})();