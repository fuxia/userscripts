// ==UserScript==
// @name         Better Editor Title
// @namespace    https://fuxia.me
// @version      0.1
// @description  Sets the currently edited document's title as HTML title
// @author       Fuxia Scholz
// @match        https://chess24.com/*/content/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    let docTitle = document.querySelector('.contentEdit h1 span');
    let regex = /\[.*\]/gi;

    if(!docTitle) {
        return;
    }

    // The main title, visible in the tab.
    document.title = '✎ ' + docTitle.textContent;
    // Make this readable.
    docTitle.classList.remove('textGrey');
    // Find the language.
    let match = regex.exec(docTitle.textContent);

    if (!match) {
        return;
    }

    // Remove it first …
    docTitle.textContent = docTitle.textContent.replace(regex, '');
    // And insert it again with style. :)
    let lang = document.createElement('b');
    lang.textContent = match;
    lang.style.color = '#07d';
    docTitle.appendChild(lang);
})();