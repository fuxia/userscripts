// ==UserScript==
// @name         Visible Opening Name
// @namespace    https://fuxia.me
// @version      0.2
// @description  Always show the opening name in the lichess analysis board.
// @author       Fuxia
// @match        https://lichess.org/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    if ( ! document.querySelector('main.analyse') ) {
        return;
    }
    let openingName = document.title.match(/: (.*) •/)[1];

    if ( 'Analysis board' == openingName ) {
        return; // Probably a variant
    }
    openingName = openingName
        .replace(': ', '<br>')
        .replace(/, /g, '<br>')
        .replace(/([A-Z]\d\d)/g, '<b style="color\:#080">$1</b>' );
    let opening = document.createElement('section');
    opening.innerHTML = openingName;
    document.querySelector('div.game__meta').appendChild(opening);
})();