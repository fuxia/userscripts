# Userscripts

These scripts require a user script manager extension in your browser. 
I recommend Tampermonkey, Greasemonkey, or Violentmonkey. You can find the 
[download links on Greasy Fork](https://greasyfork.org/en).

## chess24 Algebraic Notation

**[Install](https://gitlab.com/fuxia/userscripts/-/raw/master/chess24-algebraic-notation.user.js)**

This script is for the ebooks on chess24.com. It replaces the pictures with 
letters for the English notation. So ♘ becomes `N` and so forth.

## chess24 Auto URLs

**[Install](https://gitlab.com/fuxia/userscripts/-/raw/master/chess24-auto-url.user.js)**

This is for editors on chess24 only. It takes the title of an article and 
populates the URL field with a sanitized value.

## chess24 Better Editor Title

**[Install](https://gitlab.com/fuxia/userscripts/-/raw/master/chess24-better-editor-title.user.js)**

This is for editors on chess24 only. It sets the currently edited document's 
title as HTML title, increases the contrast of the document title and highlights 
the language name in blue.

## lichess Visible Opening Name

**[Install](https://gitlab.com/fuxia/userscripts/-/raw/master/lichess-opening.user.js)**

A while ago lichess decided not to show the opening name in the analysis board 
anymore. You can still see it if you open the database box, but it dies 
together with the database on move 26 …

Luckily, the opening name is in the document title, so you can see it there if
you happen to have a title bar. This script takes that name from the title and 
inserts it into the game meta box where it belongs.

It works on the game analysis board only, not in studies or in the generic 
analysis board.

![Screenshot Visible Opening Name](images/lichess-opening.png)
