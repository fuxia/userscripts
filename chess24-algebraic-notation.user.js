// ==UserScript==
// @name         Algebraic notation
// @namespace    https://fuxia.me/
// @version      0.1
// @description  Replaces piece symbols with algebraic notation on chess24.com
// @author       Fuxia
// @match        https://chess24.com/en/learn/advanced/ebook/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    // Thanks to Brock Adams for finding this solution: https://stackoverflow.com/a/24419809/299509
    var replaceArry = [
        [/♗/gi, 'B'],
        [/♘/gi, 'N'],
        [/♖/gi, 'R'],
        [/♕/gi, 'Q'],
        [/♔/gi, 'K'],
    ];
    var numTerms = replaceArry.length;
    var txtWalker = document.createTreeWalker (
        document.body,
        NodeFilter.SHOW_TEXT,
        {
            acceptNode: function (node) {
                //-- Skip whitespace-only nodes
                if ( node.nodeValue.trim() ) {
                    return NodeFilter.FILTER_ACCEPT;
                }

                return NodeFilter.FILTER_SKIP;
            }
        },
        false
    );
    var txtNode = null;

    while ( txtNode = txtWalker.nextNode () ) {
        var oldTxt = txtNode.nodeValue;

        for (var J = 0; J < numTerms; J++) {
            oldTxt = oldTxt.replace (replaceArry[J][0], replaceArry[J][1]);
        }
        txtNode.nodeValue = oldTxt;
    }
})();